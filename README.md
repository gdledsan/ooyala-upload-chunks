### What is this repository for? ###

* Sample script to chunk and upload a video to your Ooyala account.

### How do I get set up? ###

* Needed Gems:
require 'ooyala-v2-api'
require 'net/http'
require 'uri'
require 'rubygems'
require 'fileutils'
require 'chilkat'

* Configuration
Get you API key and Secret for your account.


* Usage:
(Comment and un comment as needed)
Option A)
Give parameters to the file:
     ruby APIuploadChunk.rb videoname,chunksize,apikey,secret
Option B)
Hardcode those in the code in the variables: 
```
#!Ruby
#maxChunkSize = 10000000 #minium is 100K 
#videoFile = "/Video/location.mov"
#name2show = "Video Name placeholder"
#apikey = ""
#secret = ""

```