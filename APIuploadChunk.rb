require 'ooyala-v2-api'
require 'net/http'
require 'uri'
require 'rubygems'
require 'fileutils'
require 'chilkat' #sudo gem install chilkat

#Data Needed

###you can either use these params to populate the needed info

videoFile = ARGV[0]
maxChunkSize = ARGV[1]
apikey = ARGV[2]
secret = ARGV[3]

### or set them here
#maxChunkSize = 10000000 #minium is 100K 
#videoFile = "/Video/location.mov"
#name2show = "Video Name placeholder"
#apikey = ""
#secret = ""


#Query params
parameters = {:include => "labels"}
path = "assets"

#Populate apikey and secret
api = Ooyala::API.new(apikey, secret)

#Splitter
fac = Chilkat::CkFileAccess.new()

# video data
video = File.open(videoFile)
videoName =  File.basename video
videoSize = File.size video
puts videoSize 
#exit
# 
fileToSplit = videoFile
partPrefix = "chunk"
#partExtension = File.extname(video.path)
partExtension = "spl"

# Dir for split files
if (Dir.exists? (Dir.getwd + "/splitvid123456789"))
    FileUtils.rm_r Dir.getwd + "/splitvid123456789" #Delete first
end
Dir.mkdir "splitvid123456789",0777
destDirPath = Dir.getwd + "/splitvid123456789"
puts "Path for split files: #{destDirPath}"

success = fac.SplitFile(fileToSplit,partPrefix,partExtension,maxChunkSize,destDirPath)

if (success == true)
    print "Success." + "\n";
else
    print fac.lastErrorText() + "\n";
end

# Start creating the asset
body = {:name => "#{name2show}",
  :file_name => "#{videoName}",
  :asset_type => "video",
  :file_size => "#{videoSize}",
  :post_processing_status=> "live",
  :chunk_size => maxChunkSize
}

#get all files from the splitfolder.
files1 = Array.new
Dir.chdir(destDirPath)
files1 = Dir.glob("*")
files1 = files1.sort_by { |item| item.to_s.split(/(\d+)/).map { |e| [e.to_i, e] } } #Human sort
        
#puts files1[2] #files start on item #2

# Api Stuff
result = api.post(path, body) #Post Asset
#get upload URL
uploadUrl = api.get("assets/#{result["embed_code"]}/uploading_urls")
#Setup the uploader
uri = URI.parse(uploadUrl[0])
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true

y = 0
uploadUrl.each do |x|
    uri = URI.parse(x)
    video2 = File.open("#{destDirPath}/#{files1[y]}")
    response = http.send_request('PUT', uri.request_uri, video2, initheader = {'Content-Length' => "#{File.size video2}"})
    
    	puts "File: #{files1[y]} Uri: #{uri} Code: #{response.code} Message: #{response.message}"
	
		#puts "File: #{video2.path} Uri: #{uri}"
	 
    y = y+1
end 
puts "Finished Uploading"
#response = http.send_request('PUT', uri.request_uri, video, initheader = {'Content-Length' => "#{videoSize}", 'Content-Type' => 'video/mp4'} )

FileUtils.rm_r destDirPath

#change status to uploaded
puts api.put("assets/#{result["embed_code"]}/upload_status", { :status => "uploaded" })
puts "#{result["embed_code"]}"